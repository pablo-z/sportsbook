var mainWrapper = document.querySelector('.main-wrapper'), 
sbIframe = document.querySelector('#sportsBookIframe'),
transferBox = document.querySelector('.sb-transfer-box'),
isShowN = document.querySelectorAll('.is-show-notification'),
shareBtn =  document.querySelector('.sb-share-button'),
shareBtnIcon = document.querySelector('.sb-share-icon'),
transferBoxPos = transferBox.getBoundingClientRect(),
transferBoxTopPos = transferBoxPos.top,
initPos = 0,
ticking = false,
currentUrl = window.location.href,
viewportHeight;


(()=>{

    window.addEventListener('scroll', (e) => {
        initPos = window.scrollY;
        if (!ticking) {
          window.requestAnimationFrame(() => {
            onScroll(initPos);
            ticking = false;
          });
        }
        ticking = true;
      });
      //sharebox
      document.querySelector('.sb-share-wsp').href = `https://api.whatsapp.com/send?text='sportsbook:%20${currentUrl}`;
      document.querySelector('.sb-share-fb').href = `https://www.facebook.com/sharer/sharer.php?u=${currentUrl}`;
      document.querySelector('.sb-share-tw').href = `https://twitter.com/intent/tweet?text=&url=${currentUrl}&hashtags=sportsbook`;
      getViewportHeight()
})();

function onScroll(ipos) {
  return ipos >= transferBoxTopPos ? mainWrapper.classList.add('sb-on-scroll') : false;
}

//notification-is-show
isShowN.forEach(item => {
  item.addEventListener('click', () =>{
    isShowNotification();
  });
});

function isShowNotification(){
  mainWrapper.classList.toggle('show-notification');
};

//share links
shareBtn.addEventListener('click', (event)=>{
  event.preventDefault();
  mainWrapper.classList.toggle('show-share-links');
  mainWrapper.classList.contains('show-share-links') ? shareBtnIcon.innerText = 'close' : shareBtnIcon.innerText = 'share'; 
});

//set iframe height
window.addEventListener('resize', getViewportHeight);

function getViewportHeight(){
  let viewportWidth = window.innerWidth;
  viewportHeight = window.innerHeight;
  return viewportWidth >= 1024 ? sbIframe.style.height = viewportHeight + 'px' : sbIframe.style.height = '668px';
}

